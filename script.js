let idBaralho;
const botaoIniciar = document.querySelector('#iniciar');
const botaoNovaCarta = document.querySelector('#novaCarta');
const botaoParar = document.querySelector('#parar');
const mesa = document.querySelector('#mesa');
let pontos = document.querySelector('#placar');
const msg = document.querySelector("#acoes p");

function extrairJson(resposta){
    return resposta.json().then();
}

function atualizarPlacar(valor){
    let total = Number(pontos.innerHTML) + valor;

    if(total === 21){
        msg.innerHTML = "Oh loro! Parabeins";
        botaoIniciar.onclick = iniciarJogo;
        botaoNovaCarta.onclick = null;
        botaoParar.onclick = null;
    }
    else if(total > 21){
        msg.innerHTML = "Errrrrooooouuuuu ";
        botaoIniciar.onclick = iniciarJogo;
        botaoNovaCarta.onclick = null;
        botaoParar.onclick = null;
    }
    else{
        pontos.innerHTML = total;
    }
}

function preencherMesa(dados){
    let cartas = dados.cards;
    for(let carta of cartas){
        desenharCarta(carta);
    }
   // console.log(cartas);
}

function iniciarMesa(dados){
    idBaralho = dados.deck_id;
    fetch(`https://deckofcardsapi.com/api/deck/${idBaralho}/draw/?count=2`).then(extrairJson).then(preencherMesa);
}

function desenharCarta(carta){
    let desenho = document.createElement("img");
    desenho.src = carta.image;
    desenho.alt = carta.code;
    mesa.appendChild(desenho);
    let valor;
    if (carta.value === "ACE"){
        valor = 1;
    }else if(carta.value === "QUEEN" || carta.value === "KING" || carta.value === "JACK"){
        valor = 10;
    }
    else{
        valor = Number(carta.value);
    }

    atualizarPlacar(valor);

}

function iniciarJogo(){
    // vai no backend buscar a informacao
    mesa.innerHTML = "";
    msg.innerHTML = "Pontuacao: <span id=\"placar\">0</span>";
    pontos = document.querySelector('#placar');
    fetch("https://deckofcardsapi.com/api/deck/new/shuffle/").then(extrairJson).then(iniciarMesa);
    botaoIniciar.onclick = null;
    botaoNovaCarta.onclick = retirarCarta;
    botaoParar.onclick = pararJogo;
}

function pararJogo(){
    msg.innerHTML = `Você parou com ${pontos.innerHTML} pontos.`;
    botaoIniciar.onclick = iniciarJogo;
    botaoNovaCarta.onclick = null;
    botaoParar.onclick = null;
}

function retirarCarta(dados){
    fetch(`https://deckofcardsapi.com/api/deck/${idBaralho}/draw/?count=1`).then(extrairJson).then(preencherMesa);
}

botaoIniciar.onclick = iniciarJogo;